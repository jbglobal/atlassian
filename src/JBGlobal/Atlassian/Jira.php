<?php

namespace JBGlobal\Atlassian;

/**
 * Atlassian Jira REST API wrapper for JBGlobal
 */
class Jira
{
    private const API_VERSION = '3';
    protected $uri;
    protected $username;
    protected $password;
    protected $encodedCredential;

    /**
     * authenticate - log in to the jira api
     *
     * @param string $uri URI
     * @param string $username Username
     * @param string $password Password
     */
    public function authenticate($uri, $username, $password)
    {
        $this->uri = $uri;
        $this->username = $username;
        $this->password = $password;
        $this->encodedCredential = base64_encode($this->username . ':' . $this->password);
    }

    /**
     * isAuthenticated - private method used to determine if we're authenticated
     *
     * @return bool
     */
    private function isAuthenticated()
    {
        if (!$this->uri or !$this->username or !$this->password) {
            echo "Not authenticated\n";
            return false;
        }
        return true;
    }

    /**
     * prepCurl - prepare the curl object
     *
     * @param string $url URL
     *
     * @return mixed
     */
    private function prepCurl($url)
    {
        $headers = array(
            'Authorization: Basic ' . $this->encodedCredential
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_PORT, '443');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

        curl_setopt($ch, CURLINFO_HEADER_OUT, true);

        return $ch;
    }

    /**
     * getFilter - retrieve a filter
     *
     * @param string $key Filter key
     *
     * @return array
     */
    public function getFilter($key)
    {
        if ($this->isAuthenticated()) {
            $url = $this->uri . "/rest/api/" . self::API_VERSION . "/filter/" . $key;
            $ch = $this->prepCurl($url);

            $responseString = curl_exec($ch);

            $curlInfo = curl_getinfo($ch);

            return json_decode($responseString, true);
        }
    }

    /**
     * getUser - retrieve a user
     *
     * @param string $accountId account id
     *
     * @return array
     */
    public function getUser($accountId)
    {
        if ($this->isAuthenticated()) {
            $url = $this->uri . "/rest/api/" . self::API_VERSION . "/user/?accountId=" . $accountId;
            $ch = $this->prepCurl($url);

            $responseString = curl_exec($ch);

            $curlInfo = curl_getinfo($ch);

            return json_decode($responseString, true);
        }
    }

    /**
     * executeFilter - run a search based on a filter
     *
     * @param string $key Filter key
     * @param array $fields Specify specific issue fields to be returned.
     *
     * @return array
     */
    public function executeFilter(string $key, array $fields = []): ?array
    {
        $response = null;

        if ($this->isAuthenticated()) {
            $url = $this->getFilter($key)['searchUrl'];

            if (! empty($fields)) {
                $url .= '&fields=' . implode(',', $fields);
            }

            $response = $this->retrieveFilterResults($url);
        }

        return $response;
    }

    /**
     * @param string $searchUrl
     * @param int $startAt
     * @return array
     */
    private function retrieveFilterResults(string $searchUrl, int $startAt = 0): array
    {
        $url = $searchUrl . '&startAt=' . $startAt;
        $ch = $this->prepCurl($url);
        $responseString = curl_exec($ch);
        $response = json_decode($responseString, true);
        $newStartAt = $this->responseHasMoreResults($response);

        if ($newStartAt > 0) {
            $moreResults = $this->retrieveFilterResults($searchUrl, $newStartAt);
            $response['issues'] = array_merge($response['issues'], $moreResults['issues']);
        }

        return $response;
    }

    /**
     * Returns a 'start at' value so you can get the next set of results.
     * @param array $response
     * @return int
     */
    private function responseHasMoreResults(array $response): int
    {
        $startAt = 0;

        if (($response['startAt'] + $response['maxResults']) < $response['total']) {
            $startAt = $response['startAt'] + $response['maxResults'];
        }

        return $startAt;
    }

    /**
     * getIssue - retrieve an issue
     *
     * @param string $key Issue key
     * @param string $fields Fields
     *
     * @return array
     */
    public function getIssue($key, $fields = false)
    {
        if ($this->isAuthenticated()) {
            // pinned to api v2 until we support adf in description fields... https://developer.atlassian.com/cloud/jira/platform/apis/document/structure/
            $url = $this->uri . "/rest/api/2/issue/" . $key;
            if ($fields) {
                $url .= "?fields=" . $fields;
            }
            $ch = $this->prepCurl($url);

            $responseString = curl_exec($ch);

            $curlInfo = curl_getinfo($ch);

            return json_decode($responseString, true);
        }
    }

    /**
     * getChangelog - retrieve the changelog for the given issue key
     *
     * @param string $key Issue key
     *
     * @return array
     */
    public function getChangelog($key)
    {
        if ($this->isAuthenticated()) {
            $url = $this->uri . "/rest/api/" . self::API_VERSION . "/issue/" . $key . "?expand=changelog";
            $ch = $this->prepCurl($url);

            $responseString = curl_exec($ch);

            $curlInfo = curl_getinfo($ch);

            return json_decode($responseString, true);
        }
    }

    /**
     * Retrieve entire changelog for an issue.
     * Can handle result sets that are greater than the max result value.
     */
    public function getChangelogV2(string $key, int $startAt = 0): array
    {
        if ($this->isAuthenticated()) {
            $url = $this->uri . "/rest/api/" . self::API_VERSION .
                "/issue/{$key}/changelog/?startAt={$startAt}&maxResults=2";
            $ch = $this->prepCurl($url);
            $responseString = curl_exec($ch);
            $curlInfo = curl_getinfo($ch);
            $response = json_decode($responseString, true);
            $newStartAt = $this->responseHasMoreResults($response);

            if ($newStartAt > 0) {
                $nextResponse = $this->getChangelogV2($key, $newStartAt);
                $response['values'] = array_merge($response['values'], $nextResponse['values']);
            }

            return $response;
        }
    }

    /**
     * Retrieve all worklogs for an issue.
     */
    public function getWorklogs(string $key, int $startAt = 0): array
    {
        if ($this->isAuthenticated()) {
            $url = $this->uri . "/rest/api/" . self::API_VERSION .
                "/issue/{$key}/worklog/?startAt={$startAt}&maxResults=2";
            $ch = $this->prepCurl($url);
            $responseString = curl_exec($ch);
            $curlInfo = curl_getinfo($ch);
            $response = json_decode($responseString, true);
            $newStartAt = $this->responseHasMoreResults($response);

            if ($newStartAt > 0) {
                $nextResponse = $this->getWorklogs($key, $newStartAt);
                $response['worklogs'] = array_merge($response['worklogs'], $nextResponse['worklogs']);
            }

            return $response;
        }
    }

    /**
     * updateIssue - update an issue
     *
     * @param string $key Issue key
     * @param array $data Data for fields
     *
     * @return array|boolean
     */
    public function updateIssue($key, $data)
    {
        if ($this->isAuthenticated()) {
            // pinned to api v2 until we support adf in description fields... https://developer.atlassian.com/cloud/jira/platform/apis/document/structure/
            $url = $this->uri . "/rest/api/2/issue/" . $key;

            $headers = array(
                'Authorization: Basic ' . $this->encodedCredential,
                'Content-Type: application/json;charset=UTF-8'
            );

            $dataString = json_encode($data);

            $ch = $this->prepCurl($url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, 10);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);
            curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            $response = curl_exec($ch);

            return $response;
        }
        return false;
    }

    /**
     * createIssue - create an issue
     *
     * @param array $data Data for fields
     *
     * @return array|boolean
     */
    public function createIssue($data)
    {
        if ($this->isAuthenticated()) {
            // pinned to api v2 until we support adf in description fields... https://developer.atlassian.com/cloud/jira/platform/apis/document/structure/
            $url = $this->uri . "/rest/api/2/issue/";

            $headers = array(
                'Authorization: Basic ' . $this->encodedCredential,
                'Content-Type: application/json;charset=UTF-8'
            );

            $dataString = json_encode($data);
            echo "DATASTRING: " . $dataString . "\n";
            $ch = $this->prepCurl($url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, 10);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);
            curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            $response = curl_exec($ch);

            file_put_contents("/tmp/robot-curl-errors.txt", var_export(curl_error($ch), true) . "\n", FILE_APPEND);
            return $response;
        }
        return false;
    }

    /**
     * deleteIssue - delete an issue
     *
     * @param string $id id of issue to delete
     *
     * @return array|boolean
     */
    public function deleteIssue($id)
    {
        if ($this->isAuthenticated()) {
            $url = $this->uri . "/rest/api/" . self::API_VERSION . "/issue/" . $id;

            $headers = array(
                'Authorization: Basic ' . $this->encodedCredential,
                'Content-Type: application/json;charset=UTF-8'
            );

            $ch = $this->prepCurl($url);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, 10);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);
            curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            $response = curl_exec($ch);

            file_put_contents("/tmp/robot-curl-errors.txt", var_export(curl_error($ch), true) . "\n", FILE_APPEND);
            return $response;
        }
        return false;
    }

    /**
     * getTransitions - retrieve a list of available transitions
     *
     * @param string $key Issue key
     *
     * @return array
     */
    public function getTransitions($key)
    {
        $url = $this->uri . "/rest/api/" . self::API_VERSION . "/issue/" . $key . '/transitions';
        $ch = $this->prepCurl($url);

        $transitions = json_decode(curl_exec($ch), true)['transitions'];

        $transitionIdToStatusName = [];

        foreach ($transitions as $transition) {
            $transitionIdToStatusName[$transition['id']] = strtolower($transition['to']['name']);
        }

        return $transitionIdToStatusName;
    }

    /**
     * transitionTo - transition an issue to a new status
     *
     * @param string $key Issue key
     * @param string $status Transition to status
     *
     * @return bool
     */
    public function transitionTo($key, $status)
    {
        $url = $this->uri . "/rest/api/" . self::API_VERSION . "/issue/" . $key . "/transitions";
        $headers = array(
            'Authorization: Basic ' . $this->encodedCredential,
            'Content-Type: application/json;charset=UTF-8'
        );

        $transitions = $this->getTransitions($key);
        if ($id = array_search(strtolower($status), $transitions)) {
            $data = ['transition' => ['id' => $id]];
            $dataString = json_encode($data, JSON_PRETTY_PRINT);

            $ch = $this->prepCurl($url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, 10);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);
            curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

            $response = curl_exec($ch);
            return true;
        }
        return false;
    }

    /**
     * addAttachment - add an attachment to an issue
     *
     * @param string $key Issue key
     * @param string $sourceFile File to attach (including path)
     * @param string $filename Filename for the attachment (no path)
     * @return bool|string
     */
    public function addAttachment(string $key, string $sourceFile, string $filename)
    {
        $url = $this->uri . "/rest/api/" . self::API_VERSION . "/issue/" . $key . "/attachments";
        $headers = array(
            'Authorization: Basic ' . $this->encodedCredential,
            'X-Atlassian-Token: no-check'
        );

        $fields = [
            'file' => new \CurlFile($sourceFile, mime_content_type($sourceFile), $filename)
        ];

        $ch = $this->prepCurl($url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 300);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);
        curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        return curl_exec($ch);
    }

    /**
     * deleteAttachment - delete an attachment
     *
     * @param int $attachmentId Attachment ID
     * @return bool|string
     */
    public function deleteAttachment(int $attachmentId)
    {
        $url = $this->uri . "/rest/api/" . self::API_VERSION . "/attachment/" . $attachmentId;
        $headers = [
            'Authorization: Basic ' . $this->encodedCredential
        ];

        $ch = $this->prepCurl($url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 300);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);
        curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        return curl_exec($ch);
    }
    
    /**
     * addComment - add a comment to an issue
     *
     * @param string $key Issue key
     * @param string $comment Text of the comment
     *
     * @return array
     */
    public function addComment($key, $comment)
    {
        $url = $this->uri . "/rest/api/" . self::API_VERSION . "/issue/" . $key . "/comment";
        $headers = array(
            'Authorization: Basic ' . $this->encodedCredential,
            'Content-Type: application/json;charset=UTF-8'
        );

        $data = ['body' => $comment];
        $dataString = json_encode($data, JSON_PRETTY_PRINT);

        $ch = $this->prepCurl($url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);
        curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $response = curl_exec($ch);

        return $response;
    }

    /**
     * runQuery - run an arbitrary jql query
     *
     * @param string $jql
     * @param int $startAt
     *
     * @return array
     */
    public function runQuery(string $jql, int $startAt = 0): array
    {
        if ($this->isAuthenticated()) {
            $jql = preg_replace("/ /", "+", $jql);
            $url = $this->uri . "/rest/api/" . self::API_VERSION . "/search?jql={$jql}&startAt={$startAt}";
            $ch = $this->prepCurl($url);
            $responseString = curl_exec($ch);
            $curlInfo = curl_getinfo($ch);
            $response = json_decode($responseString, true);
            $newStartAt = $this->responseHasMoreResults($response);

            if ($newStartAt > 0) {
                $nextResponse = $this->runQuery($jql, $newStartAt);
                $response['issues'] = array_merge($response['issues'], $nextResponse['issues']);
            }

            return $response;
        }
    }
}
